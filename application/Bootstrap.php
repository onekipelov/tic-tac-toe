<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    /**
     * @return Zend_Registry
     */
    protected function _initRegistry()
    {
        $registry = Zend_Registry::getInstance();
        return $registry;
    }

    /**
     * @return Zend_Application_Module_Autoloader
     */
    protected function _initAutoload()
    {
        $autoloader = new Zend_Application_Module_Autoloader(array(
            'namespace' => 'Default_',
            'basePath' => dirname(__FILE__),
        ));
        return $autoloader;
    }

    /**
     * @return Doctrine_Manager
     */
    public function _initDoctrine()
    {
        // include loader
        require_once('Doctrine/Common/ClassLoader.php');
        $classLoader = new \Doctrine\Common\ClassLoader(
            'Doctrine',
            APPLICATION_PATH . '/../library/'
        );
        $classLoader->register();

        // Doctrine config
        $config = new \Doctrine\ORM\Configuration();

        // ArrayCache
        $cache = new \Doctrine\Common\Cache\ArrayCache;
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);

        $driver = $config->newDefaultAnnotationDriver(
            APPLICATION_PATH . '/models'
        );
        $config->setMetadataDriverImpl($driver);

        // Proxy
        $config->setProxyDir(APPLICATION_PATH . '/models/Proxies');
        $config->setAutoGenerateProxyClasses(true);
        $config->setProxyNamespace('App\Proxies');

        // EntityManager
        $connectionSettings = $this->getOption('doctrine');
        $conn = array(
            'driver'   => $connectionSettings['conn']['driv'],
            'user'     => $connectionSettings['conn']['username'],
            'password' => $connectionSettings['conn']['password'],
            'dbname'   => $connectionSettings['conn']['dbname'],
            'host'     => $connectionSettings['conn']['host']
        );
        $entityManager = \Doctrine\ORM\EntityManager::create($conn, $config);

        $registry = Zend_Registry::getInstance();
        $registry->entitymanager = $entityManager;

        return $entityManager;
    }

    protected function _initHelper()
    {
        // addPath(path_to_helper, helper_prefix)
        Zend_Controller_Action_HelperBroker::addPrefix('Helpers_App');
    }

    protected function _initDatabase()
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'production');
        // setup database
        $db = Zend_Db::factory($config->db->adapter, $config->doctrine->conn->toArray());
        Zend_Db_Table::setDefaultAdapter($db);
    }

    protected function _initGlobals()
    {
        $config = $this->getOptions();
        // players states
        define('PLAYER_OFFLINE', $config['app']['player']['offline']);
        define('PLAYER_BUSY', $config['app']['player']['busy']);
        define('PLAYER_FREE', $config['app']['player']['free']);

        // games states
        define('GAME_AWAITING', $config['app']['game']['awaiting']);
        define('GAME_INPROGRESS', $config['app']['game']['inprogress']);
        define('GAME_PAUSED', $config['app']['game']['paused']);
        define('GAME_FINISHED', $config['app']['game']['finished']);
        define('GAME_CANCELED', $config['app']['game']['canceled']);

        // game routine states (for client - server requests)
        define('GSTATUS_READY', $config['app']['gstatus']['ready']);
        define('GSTATUS_CHALLENGE_SENT', $config['app']['gstatus']['challenge_sent']);
        define('GSTATUS_CHALLENGE_RECEIVED', $config['app']['gstatus']['challenge_received']);
        define('GSTATUS_INPROGRESS', $config['app']['gstatus']['inprogress']);
        define('GSTATUS_CANCELED', $config['app']['gstatus']['canceled']);
        define('GSTATUS_ACCEPTED', $config['app']['gstatus']['accepted']);
        define('GSTATUS_FINISHED', $config['app']['gstatus']['finished']);
    }

}
