<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setMethod('post');

        $this->addElement('text', 'userName', array(
            'label'   => 'Login:',
            'filters'  => array('StringTrim')
        ));
        $el = $this->getElement('userName');
        $el->setRequired(true)
            ->addValidators(array(
                array('NotEmpty', true, array('messages' => array(
                    'isEmpty' => 'login is required',
                )))));


        $this->addElement('password', 'password', array(
            'label'   => 'Password:'
        ));
        $el = $this->getElement('password');

        $el->setRequired(true)->addValidators(array(
            array('NotEmpty', true, array('messages' => array(
                'isEmpty' => 'password is requires',
            )))));

        $this->addElement('submit', 'login', array(

            'label'   => 'Login'
        ));
    }

}

