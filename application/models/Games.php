<?php

/**
 * @Entity
 * @Table(name="Game")
 */
class Game
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="integer", nullable=false)
     */
    private $p1;

    /**
     * @Column(type="integer", nullable=false)
     */
    private $p2;

    /**
     * @Column(type="integer", nullable=false)
     */
    private $turn;

    /**
     * @Column(type="string", nullable=true, length=128)
     */
    private $game_field; // NULL - field empty; o - contains 0; x - contains X

    /**
     * @Column(type="datetime")
     */
    private $start;

    /**
     * @Column(type="datetime", nullable=true)
     */
    private $end;

    /**
     * @Column(type="integer", nullable=true)
     */
    private $won;

    /**
     * @Column(type="integer", nullable=true)
     */
    private $status; // 1 - started; 2 - ended

}
