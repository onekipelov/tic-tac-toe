<?php

/**
 * @Entity
 * @Table(name="User")
 */
class User
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", nullable=false, length=64)
     */
    private $name;

    /**
     * @Column(type="string", nullable=false, length=16)
     */
    private $login;

    /**
     * @Column(type="string", length=48, nullable=false)
     */
    private $password;

    /**
     * @Column(type="datetime")
     */
    private $last_visit;
    /**
     * @Column(type="integer", nullable=false)
     */
    private $online_status; // 0 - offline; 1 - online



//    public function setName($string)
//    {
//        $this->name = $string;
//        return true;
//    }
}
