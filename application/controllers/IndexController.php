<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $registry = Zend_Registry::getInstance();
        $this->_em = $registry->entitymanager;


        // update online status
        if (Zend_Auth::getInstance()->hasIdentity()) {
            $this->userStateHelper = $this->_helper->getHelper('UserState');
            $this->userStateHelper->updateOnlineStatus();
        }
    }

    public function indexAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            return $this->_redirect('/login');
        }
        $auth = Zend_Auth::getInstance();
        $identity = Zend_Auth::getInstance()->getIdentity();

        if(!empty($identity->name))
        {
            $this->view->name = $identity->name;
        }

        $this->view->auth = $auth;
    }

    public function resultsAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            return $this->_redirect('/login');
        }
        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

        // get my today results
        $gameRoutineHelper = $this->_helper->getHelper('GameRoutine');
        $myTodayResults = $gameRoutineHelper->getMyTodayResults();

        $this->view->myWins = $myTodayResults['myWins'];
        $this->view->myLoses = $myTodayResults['myLoses'];
        $this->view->myDraws = $myTodayResults['myDraws'];

        // get my total results
        $myTotalResults = $gameRoutineHelper->getMyTotalResults();
        $this->view->myTotalWins = $myTotalResults['myTotalWins'];
        $this->view->myTotalLoses = $myTotalResults['myTotalLoses'];
        $this->view->myTotalDraws = $myTotalResults['myTotalDraws'];

    }

    public function checkrequestAction()
    {
        $this->_helper->layout->disableLayout();
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $result['error'] = "Authentication lost";
            echo json_encode($result);
            return;
        }

        // get online players
        $onlinePlayers = $this->userStateHelper->getOnlinePlayersList();
        $result['onlinePlayers'] = $onlinePlayers;

        // game routine
        $gameRoutineHelper = $this->_helper->getHelper('GameRoutine');
        $gameState = $gameRoutineHelper->runGame();

        $result['gameState'] = $gameState;

        echo json_encode($result);
    }

}
