<?php

class LoginController extends Zend_Controller_Action
{

    public function init()
    {

    }

    public function indexAction()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            return $this->_redirect('/');
        }

        $form = $this->_getLoginForm();

        if ($this->_request->isPost()) {
            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {

                $auth  = Zend_Auth::getInstance();
                $authAdapter = $this->_getAuthAdapter($formData['userName'], sha1($formData['password']));
                $result = $auth->authenticate($authAdapter);
                if (!$result->isValid()) {
                    // wrong credentials
                    $form->setDescription('Login or password is not correct');
                    $form->populate($formData);
                    $this->view->form = $form;
                    return $this->render('index');
                } else {

                    $currentUser = $authAdapter->getResultRowObject();
                    Zend_Auth::getInstance()->getStorage()->write( $currentUser);

                    // set ready user status
                    $registry = Zend_Registry::getInstance();
                    $em = $registry->entitymanager;
                    $userId = intval($auth->getIdentity()->id);
                    $user = $em->getRepository('Application\Entity\User')->find($userId);

                    if(!empty($user)) {
                        $user->setOnlineStatus(PLAYER_FREE); // 2 - ready
                        $em->persist($user);
                        $em->flush();
                    }
                    return $this->_redirect('/');
                }
            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $auth  = Zend_Auth::getInstance();

        // set offline user status
        $userId = intval($auth->getIdentity()->id);
        $user = $em->getRepository('Application\Entity\User')->find($userId);

        if(!empty($user)) {
            $user->setOnlineStatus(PLAYER_OFFLINE); // 0 - offline
            $em->persist($user);
            $em->flush();
        }

        $auth->clearIdentity();
        Zend_Session::forgetMe();
        Zend_Session::expireSessionCookie();
        return $this->_redirect('/');
    }

    protected function _getLoginForm()
    {
        return new Application_Form_Login();
    }

    protected function _getAuthAdapter($userName, $userPassword)
    {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable(
            $dbAdapter,
            'User',
            'login',
            'password'
        );
        $authAdapter->setIdentity($userName)->setCredential($userPassword);

        return $authAdapter;
    }

}

