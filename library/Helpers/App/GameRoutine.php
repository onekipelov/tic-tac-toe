<?php

class Helpers_App_GameRoutine extends Zend_Controller_Action_Helper_Abstract
{
    public function runGame()
    {
        $gameState = array();

        $auth = Zend_Auth::getInstance();
        $userId = intval($auth->getIdentity()->id);

        $userStateHelper = $this->_actionController->getHelper('UserState');

        // analyse game status
        $gameStatus = intval($this->getRequest ()->getParam ( 'game_status' ));

        // get game id
        $gameId = intval($this->getRequest ()->getParam ( 'game_id' ));

        switch($gameStatus) { // GSTATUS_READY 0 - ready to game; 1 GSTATUS_CHALLENGE_SENT - sent challenge;
            // GSTATUS_CHALLENGE_RECEIVED 2 - received challenge; GSTATUS_INPROGRESS 3 - game in progress; GSTATUS_CANCELED 4 -canceled
            // GSTATUS_ACCEPTED 5 - accepted; GSTATUS_FINISHED 6 - finished
            case GSTATUS_CHALLENGE_SENT : {
                // sent challenge - check for player available
                $opponentId = intval($this->getRequest ()->getParam ( 'invited_player' ));

                if(!empty($gameId)) {
                    // game exist. check created game status
                    $currentGame = $this->getGameById($gameId);

                    $gameStatus = $currentGame->getStatus();
                    if($gameStatus == GAME_CANCELED) {
                        // cancel game
                        $gameState['game_id'] = 0;
                        $gameState['game_status'] = GSTATUS_CANCELED;
                        $gameState['invited_player'] = 0;
                        $gameState['pretender_player'] = 0;
                    } elseif($gameStatus == GAME_INPROGRESS) {
                        // if game has begin then change status to inprogress
                        $gameState['game_status'] = GSTATUS_INPROGRESS;
                        $gameState['invited_player'] = 0;
                        $gameState['pretender_player'] = 0;
                        $gameState['game_id'] = $gameId;
                    }
                    else {
                        $gameState['game_status'] = GSTATUS_CHALLENGE_SENT;
                        $gameState['invited_player'] = $opponentId;
                        $gameState['game_id'] = $gameId;
                    }
                } else {
                    // create new game
                    // get opponent status
                    $opponentStatus = $userStateHelper->getPlayerStatus($opponentId);

                    if($opponentStatus == PLAYER_FREE) {
                        // check opponent unfinished games
                        $opponentUnfinishedGames = $this->getPlayerUnfinishedGames($opponentId);
                        if(empty($opponentUnfinishedGames) ) {

                            // create game with opponent
                            $gameId = $this->createGame($userId, $opponentId);

                            $gameState['game_status'] = GSTATUS_CHALLENGE_SENT;
                            $gameState['invited_player'] = $opponentId;
                            $gameState['game_id'] = $gameId;
                        }
                    }
                }
                // send opponent info
                $opponent = $userStateHelper->getPlayer($opponentId);
                if(!empty($opponent)) {
                    $gameState['opponent']['id'] = $opponent->getId();
                    $gameState['opponent']['name'] = $opponent->getName();
                }

                break;
            }
            case GSTATUS_CHALLENGE_RECEIVED : {
                // received challenge
                // check for invitation to challenge
                $userUnfinishedGames = $this->getPlayerUnfinishedGames($userId);
                if(!empty($userUnfinishedGames[0])) {
                    if($userUnfinishedGames[0]->getStatus() == GAME_AWAITING && $userUnfinishedGames[0]->getP2() == $userId) {
                        $invitationReceived = $userUnfinishedGames[0]->getP1();
                    }
                }

                if(!empty($invitationReceived)) {
                    $gameState['game_status'] = GSTATUS_CHALLENGE_RECEIVED;
                    $gameState['game_id'] = $userUnfinishedGames[0]->getId();

                    // send pretender info
                    $pretender = $userStateHelper->getPlayer($invitationReceived);
                    if(!empty($pretender)) {
                        $gameState['pretender']['id'] = $pretender->getId();
                        $gameState['pretender']['name'] = $pretender->getName();
                    }
                } else {
                    // invitation canceled
                    $gameState['game_id'] = 0;
                    $gameState['game_status'] = GSTATUS_CANCELED;
                    $gameState['invited_player'] = 0;
                    $gameState['pretender_player'] = 0;
                }
                break;
            }
            case GSTATUS_INPROGRESS : {
                // check game for end
                $game = $this->getGameById($gameId);
                if($game->getStatus() == GAME_FINISHED) {
                    // stop game processing

                    // get self info
                    $self = $userStateHelper->getPlayer($userId);

                    // get opponent info and set won
                    if($game->getP1() == $userId) {
                        $opponentId = $game->getP2();
                    } else {
                        $opponentId = $game->getP1();
                    }
                    $opponent = $userStateHelper->getPlayer($opponentId);

                    // set won
                    if($game->getWon() == 0) {
                        $gameState['won'] = 'draw';
                    }
                    elseif($userId == $game->getWon()) {
                        $gameState['won'] = 'self';
                    } else {
                        $gameState['won'] = 'opponent';
                    }

                    // free status for self
                    $userStateHelper->setPlayerStatus($userId, PLAYER_FREE);

                    // set players info
                    $gameState['self']['id'] = $userId;
                    $gameState['self']['name'] = $self->getName();

                    $gameState['opponent']['id'] = $opponentId;
                    $gameState['opponent']['name'] = $opponent->getName();

                    $gameState['game_status'] = GSTATUS_FINISHED;
                    $gameState['game_id'] = 0;

                    break;
                }


                $gameField = $this->getRequest ()->getParam ( 'game_field' );
                $turn = $this->getRequest ()->getParam ( 'turn' );
                if(!empty($gameField)) {
                    // analyse game conditions for end
                    /*
                     *   1|2|3
                     *   4|5|6
                     *   7|8|9
                     */
                    $symbol = 'x';
                    $won = false;
                    if(($gameField[1] == $symbol && $gameField[2] == $symbol && $gameField[3] == $symbol)  ||
                        ($gameField[4] == $symbol && $gameField[5] == $symbol && $gameField[6] == $symbol) ||
                        ($gameField[7] == $symbol && $gameField[8] == $symbol && $gameField[9] == $symbol) ||
                        ($gameField[1] == $symbol && $gameField[4] == $symbol && $gameField[7] == $symbol) ||
                        ($gameField[2] == $symbol && $gameField[5] == $symbol && $gameField[8] == $symbol) ||
                        ($gameField[3] == $symbol && $gameField[6] == $symbol && $gameField[9] == $symbol) ||
                        ($gameField[1] == $symbol && $gameField[5] == $symbol && $gameField[9] == $symbol) ||
                        ($gameField[3] == $symbol && $gameField[5] == $symbol && $gameField[7] == $symbol)
                    ) {
                        $won = $symbol;
                    } elseif($gameField[1] != "0" && $gameField[2] != "0" && $gameField[3] != "0" &&
                        $gameField[4] != "0" && $gameField[5] != "0" && $gameField[6] != "0"      &&
                        $gameField[7] != "0" && $gameField[8] != "0" && $gameField[9] != "0") {
                        // draw
                        $won = 'draw';
                    } else {
                        $symbol = 'o';
                        if(($gameField[1] == $symbol && $gameField[2] == $symbol && $gameField[3] == $symbol)  ||
                            ($gameField[4] == $symbol && $gameField[5] == $symbol && $gameField[6] == $symbol) ||
                            ($gameField[7] == $symbol && $gameField[8] == $symbol && $gameField[9] == $symbol) ||
                            ($gameField[1] == $symbol && $gameField[4] == $symbol && $gameField[7] == $symbol) ||
                            ($gameField[2] == $symbol && $gameField[5] == $symbol && $gameField[8] == $symbol) ||
                            ($gameField[3] == $symbol && $gameField[6] == $symbol && $gameField[9] == $symbol) ||
                            ($gameField[1] == $symbol && $gameField[5] == $symbol && $gameField[9] == $symbol) ||
                            ($gameField[3] == $symbol && $gameField[5] == $symbol && $gameField[7] == $symbol)

                        ) {
                            $won = $symbol;
                        }
                    }
                }

                if(!empty($won)) {
                    // end the game
                    $params['status'] = GAME_FINISHED;
                    $params['end'] = date("Y-m-d H:i:s");
                    if($won == 'draw') {
                        $params['won'] = 'draw';
                    } else {
                        if($userId == $game->getP1()) {
                            $params['won'] = $userId;
                        } else {
                            $params['won'] = $game->getP2();;
                        }
                    }
                    $game = $this->updateGame($gameId, $params);

                    $gameState['game_status'] = GSTATUS_FINISHED;
                    $gameState['game_id'] = 0;

                    // get self info
                    $self = $userStateHelper->getPlayer($userId);

                    // get opponent info and set won
                    if($game->getP1() == $userId) {
                        $opponentId = $game->getP2();
                    } else {
                        $opponentId = $game->getP1();
                    }
                    $opponent = $userStateHelper->getPlayer($opponentId);

                    // set won
                    if($game->getWon() == 0) {
                        $gameState['won'] = 'draw';
                    }
                    elseif($userId == $game->getWon()) {
                        $gameState['won'] = 'self';
                    } else {
                        $gameState['won'] = 'opponent';
                    }

                    // free status for self
                    $userStateHelper->setPlayerStatus($userId, PLAYER_FREE);

                    // set players info
                    $gameState['self']['id'] = $userId;
                    $gameState['self']['name'] = $self->getName();

                    $gameState['opponent']['id'] = $opponentId;
                    $gameState['opponent']['name'] = $opponent->getName();

                } else {
                    //game in progress
                    $gameState['game_status'] = GSTATUS_INPROGRESS;
                    $gameState['game_id'] = $gameId;

                    // update game
                    $params['status'] = GAME_INPROGRESS;
                    $game = $this->updateGame($gameId, $params);

                    // if self turn,  check next turn
                    if($turn == 'self') {
                        $savedGameField = json_decode($game->getGameField());
                        $savedGameFieldArray = array();
                        foreach($savedGameField as $key => $value) {
                            $savedGameFieldArray[$key] = $value;
                        }
                        // check send and saved field different
                        if(!empty($gameField)) {
                            if($gameField !== $savedGameFieldArray) {
                                // next player turn
                                if($game->getP1() == $userId) {
                                    // i'm player 1
                                    $params['turn'] = $game->getP2();
                                } else {
                                    // i'm player 2
                                    $params['turn'] = $game->getP1();
                                }
                                    $params['game_field'] = json_encode($gameField);

                                $game = $this->updateGame($gameId, $params);
                            }
                        }
                    }

                    if($game->getStatus() != GAME_INPROGRESS) {
                        $gameState['game_status'] = GSTATUS_READY;
                        break;
                    }
                    // get self info
                    $self = $userStateHelper->getPlayer($userId);

                    // get opponent info and self user symbol
                    if($game->getP1() == $userId) {
                        $opponentId = $game->getP2();

                        // set self user symbol (p1 - x)
                        $gameState['user_symbol'] = "x";
                    } else {
                        $opponentId = $game->getP1();

                        // set self user symbol (p2 - o)
                        $gameState['user_symbol'] = "o";
                    }
                    $opponent = $userStateHelper->getPlayer($opponentId);

                    // set players info
                    $gameState['self']['id'] = $userId;
                    $gameState['self']['name'] = $self->getName();

                    $gameState['opponent']['id'] = $opponentId;
                    $gameState['opponent']['name'] = $opponent->getName();

                    $turn = $game->getTurn();
                    if($turn == $userId) {
                        $turn = 'self';
                    } else {
                        $turn ='opponent';
                    }
                    $gameState['turn'] = $turn;

                    // set game field
                    $gameField = $game->getGameField();
                    if(!empty($gameField)) {
                        $gameState['game_field'] = json_decode($gameField);
                    } else {
                        // game field setting up first time
                        $gameField = array(
                            1       => '0',
                            2       => '0',
                            3       => '0',
                            4       => '0',
                            5       => '0',
                            6       => '0',
                            7       => '0',
                            8       => '0',
                            9       => '0',
                        );
                        $gameState['game_field'] = $gameField;
                    }
                }

                break;
            }
            case GSTATUS_CANCELED : {
                //game canceled
                $opponentId = intval($this->getRequest ()->getParam ( 'invited_player' ));
                $pretenderId = intval($this->getRequest ()->getParam ( 'pretender_player' ));
                if(!empty($opponentId)) {
                    $this->cancelGame($userId, $opponentId);
                    $gameState['game_status'] = GSTATUS_READY;
                } elseif(!empty($pretenderId)) {
                    $this->cancelGame($pretenderId, $userId);
                    $gameState['game_status'] = GSTATUS_READY;
                }

                $gameState['invited_player'] = 0;
                $gameState['pretender_player'] = 0;
                break;
            }
            case GSTATUS_ACCEPTED : {
                // launch the game
                $params['status'] = GAME_INPROGRESS;
                $game = $this->updateGame($gameId, $params);

                $gameState['turn'] = $game->getTurn();
                $gameState['game_field'] = $game->getGameField();
                $gameState['game_status'] = GSTATUS_INPROGRESS;
                $gameState['game_id'] = $gameId;
                break;
            }
            default : {
                $userUnfinishedGames = $this->getPlayerUnfinishedGames($userId);

                // check for invitation to challenge
                $invitationReceived = $this->checkForInvitation($userId);
                if($invitationReceived) {
                    $gameState['game_status'] = GSTATUS_CHALLENGE_RECEIVED;
                    if(!empty($userUnfinishedGames[0])) {
                        $gameState['game_id'] = $userUnfinishedGames[0]->getId();
                    }

                    // send pretender info
                    $pretender = $userStateHelper->getPlayer($invitationReceived);
                    if(!empty($pretender)) {
                        $gameState['pretender']['id'] = $pretender->getId();
                        $gameState['pretender']['name'] = $pretender->getName();
                    }
                } elseif(!empty($userUnfinishedGames[0])) {
                    // check for send challenge
                    if($userUnfinishedGames[0]->getP1() == $userId && $userUnfinishedGames[0]->getStatus() == GAME_AWAITING) {
                        // restore invitation info
                        $gameState['game_status'] = GSTATUS_CHALLENGE_SENT;
                        $gameState['invited_player'] = $userUnfinishedGames[0]->getP2();
                        $gameState['game_id'] = $userUnfinishedGames[0]->getId();

                        // send opponent info
                        $opponent = $userStateHelper->getPlayer($gameState['invited_player']);
                        if(!empty($opponent)) {
                            $gameState['opponent']['id'] = $opponent->getId();
                            $gameState['opponent']['name'] = $opponent->getName();
                        }
                    } elseif($userUnfinishedGames[0]->getStatus() == GAME_INPROGRESS) {
                        $gameState['game_status'] = GSTATUS_INPROGRESS;
                        $gameState['game_id'] = $userUnfinishedGames[0]->getId();
                    } else {
                        $gameState['game_status'] = GSTATUS_READY;
                    }
                }
            }
        }

        return $gameState;
    }

    public function getPlayerUnfinishedGames($playerId)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $q = $em->createQuery("
            select
                g
            from Application\Entity\Game g
            where (g.p1 = $playerId or g.p2 = $playerId) and g.status < :status
        ")
            ->SetParameters(array(
                'status'                      => GAME_FINISHED,
            ));
        $unfinishedGames = $q->getResult();

        return $unfinishedGames;
    }

    public function createGame($userId, $opponentId)
    {
        $userStateHelper = $this->_actionController->getHelper('UserState');

        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        // set opponent status in game (busy)
        $userStateHelper->setPlayerStatus($opponentId, PLAYER_BUSY);

        // set self status in game (busy)
        $userStateHelper->setPlayerStatus($userId, PLAYER_BUSY);

        // create game
        $game = new Application\Entity\Game;

        if(!empty($game)) {
            $game->setP1($userId);
            $game->setP2($opponentId);
            $game->setStart(date("Y-m-d H:i:s"));
            $turn = rand(0, 1);
            if($turn == 0) {
                $game->setTurn($userId);
            } else {
                $game->setTurn($opponentId);
            }
            $game->setStatus(GAME_AWAITING);
            $em->persist($game);
            $em->flush();
            return $game->getId();
        }

        return false;
    }

    public function cancelGame($userId, $opponentId)
    {
        $userStateHelper = $this->_actionController->getHelper('UserState');

        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $q = $em->createQuery("
            select
                g
            from Application\Entity\Game g
            where g.p1 = $userId and g.p2 = $opponentId and g.status < :status
        ")
            ->SetParameters(array(
                'status'                      => GAME_FINISHED,
            ));
        $game = $q->getResult();
//        var_dump($game);
        if(!empty($game[0])) {
            $gameId = $game[0]->getId();

            $gameForCancel = $em->getRepository('Application\Entity\Game')->findOneBy(array(
                'id'        => $gameId,
            ));

            if(!empty($gameForCancel)) {
                $gameForCancel->setEnd(date("Y-m-d H:i:s"));
                $gameForCancel->setStatus(GAME_CANCELED); // 4 - canceled
                $em->persist($gameForCancel);
                $em->flush();
            }
        }

        // set opponent status to free
        $userStateHelper->setPlayerNewStatusIfStaus($opponentId, PLAYER_FREE, PLAYER_BUSY);

        // set self status to free
        $userStateHelper->setPlayerNewStatusIfStaus($userId, PLAYER_FREE, PLAYER_BUSY);

    }

    public function checkForInvitation($userId)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        // try to find invitation
        $invitationToGame = $em->getRepository('Application\Entity\Game')->findOneBy(array(
            'p2'        => $userId,
            'status'    => GAME_AWAITING
        ));

        if(!empty($invitationToGame)) {
            return $invitationToGame->getP1();
        }
        return false;
    }

    public function getGameById($gameId)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $game = $em->getRepository('Application\Entity\Game')->findOneBy(array(
            'id'        => $gameId,
        ));

        return $game;
    }

    public function updateGame($gameId, $params)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $game = $em->getRepository('Application\Entity\Game')->findOneBy(array(
            'id'        => $gameId,
        ));

        if(!empty($game)) {
            if(!empty($params['end'])) {
                $game->setEnd($params['end']);
            }
            if(!empty($params['status'])) {
                $game->setStatus($params['status']);
            }
            if(!empty($params['game_field'])) {
                $game->setGameField($params['game_field']);
            }
            if(!empty($params['turn'])) {
                $game->setTurn($params['turn']);
            }
            if(!empty($params['won'])) {
                $game->setWon($params['won']);
            }
            $em->persist($game);
            $em->flush();
        }

        return $game;
    }

    public function getMyTodayResults()
    {
        $auth = Zend_Auth::getInstance();
        $userId = intval($auth->getIdentity()->id);

        // get my today games
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $q = $em->createQuery('
            select
                g
            from Application\Entity\Game g
            where (g.p1 = :userId or g.p2 = :userId) and g.end >= :dateStart and g.end <= :dateEnd and g.status = :status
        ')
            ->SetParameters(array(
                'userId'                        => $userId,
                'dateStart'                     => date("Y-m-d 00:00:00"),
                'dateEnd'                       => date("Y-m-d 23:59:59"),
                'status'                        => GAME_FINISHED,
            ));
        $results = $q->getResult();

        $myWins = 0;
        $myLoses = 0;
        $myDraws = 0;
        foreach($results as $result){
            // check wins
            if($result->getWon() == $userId) {
                $myWins++;
            } elseif($result->getWon() == 0) {
                $myDraws++;
            } else {
                $myLoses++;
            }
        }

        $myTodayResults = array(
            'myWins'             => $myWins,
            'myLoses'            => $myLoses,
            'myDraws'            => $myDraws,
        );

        return $myTodayResults;
    }

    public function getMyTotalResults() {
        $auth = Zend_Auth::getInstance();
        $userId = intval($auth->getIdentity()->id);

        // get my today games
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $q = $em->createQuery('
            select
                g
            from Application\Entity\Game g
            where (g.p1 = :userId or g.p2 = :userId) and g.status = :status
        ')
            ->SetParameters(array(
                'userId'                        => $userId,
                'status'                        => GAME_FINISHED,
            ));
        $results = $q->getResult();

        $myTotalWins = 0;
        $myTotalLoses = 0;
        $myTotalDraws = 0;
        foreach($results as $result){
            // check wins
            if($result->getWon() == $userId) {
                $myTotalWins++;
            } elseif($result->getWon() == 0) {
                $myTotalDraws++;
            } else {
                $myTotalLoses++;
            }
        }

        $myTotalResults = array(
            'myTotalWins'               => $myTotalWins,
            'myTotalLoses'              => $myTotalLoses,
            'myTotalDraws'              => $myTotalDraws,
        );

        return $myTotalResults;
    }

}
