<?php

class Helpers_App_UserState extends Zend_Controller_Action_Helper_Abstract
{

    public function updateOnlineStatus()
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $auth = Zend_Auth::getInstance();
        $userId = intval($auth->getIdentity()->id);

        $user = $em->getRepository('Application\Entity\User')->find($userId);

        $user->setLastVisit(date("Y-m-d H:i:s"));
        $em->persist($user);
        $em->flush();

    }

    public function getOnlinePlayersList()
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $auth = Zend_Auth::getInstance();
        $userId = intval($auth->getIdentity()->id);

        $q = $em->createQuery("
            select
                u.id,
                u.name
            from Application\Entity\User u where u.id <> $userId and u.last_visit > :time and u.online_status = :online_status
        ")
            ->SetParameters(array(
                'time'                      => date('Y-m-d H:i:s' , time() - (60 * 2)),
                'online_status'             => PLAYER_FREE,
            )); // sub 2 - minutes
        $players = $q->getResult();

        return $players;

    }

    public function getPlayerStatus($userId)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $q = $em->createQuery("
            select
                u.online_status
            from Application\Entity\User u where u.id = $userId and u.last_visit > :time and u.online_status <> :online_status
        ")
            ->SetParameters(array(
                'time'                      => date('Y-m-d H:i:s' , time() - (60 * 2)),
                'online_status'             => PLAYER_OFFLINE,
            )); // sub 2 - minutes
        $onlineStatus = $q->getResult();

        if(!empty($onlineStatus[0]['online_status'])) {
            return $onlineStatus[0]['online_status'];
        } else {
            return 0;
        }
    }

    public function setPlayerStatus($playerId, $status)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $player = $em->getRepository('Application\Entity\User')->find($playerId);

        if(!empty($player)) {
            $player->setOnlineStatus($status);
            $em->persist($player);
            $em->flush();
        }
    }

    public function setPlayerNewStatusIfStaus($playerId, $newStatus, $currentStatus)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $player = $em->getRepository('Application\Entity\User')->findOneBy(array(
            'id'                => $playerId,
            'online_status'     => $currentStatus,
        ));

        if(!empty($player)) {
            $player->setOnlineStatus($newStatus);
            $em->persist($player);
            $em->flush();
        }
    }

    public function getPlayer($playerId)
    {
        $registry = Zend_Registry::getInstance();
        $em = $registry->entitymanager;

        $player = $em->getRepository('Application\Entity\User')->findOneBy(array(
            'id'                => $playerId,
        ));

        return $player;
    }

}