<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Application\Entity\Game
 *
 * @Table(name="Game")
 * @Entity
 */
class Game
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer $p1
     *
     * @Column(name="p1", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $p1;

    /**
     * @var integer $p2
     *
     * @Column(name="p2", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $p2;

    /**
     * @var integer $turn
     *
     * @Column(name="turn", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $turn;

    /**
     * @var string $game_field
     *
     * @Column(name="game_field", type="string", length=128, precision=0, scale=0, nullable=true, unique=false)
     */
    private $game_field;

    /**
     * @var datetime $start
     *
     * @Column(name="start", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $start;

    /**
     * @var datetime $end
     *
     * @Column(name="end", type="string", precision=0, scale=0, nullable=true, unique=false)
     */
    private $end;

    /**
     * @var integer $won
     *
     * @Column(name="won", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $won;

    /**
     * @var integer $status
     *
     * @Column(name="status", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private $status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set p1
     *
     * @param integer $p1
     * @return Game
     */
    public function setP1($p1)
    {
        $this->p1 = $p1;
        return $this;
    }

    /**
     * Get p1
     *
     * @return integer 
     */
    public function getP1()
    {
        return $this->p1;
    }

    /**
     * Set p2
     *
     * @param integer $p2
     * @return Game
     */
    public function setP2($p2)
    {
        $this->p2 = $p2;
        return $this;
    }

    /**
     * Get p2
     *
     * @return integer 
     */
    public function getP2()
    {
        return $this->p2;
    }

    /**
     * Set turn
     *
     * @param integer $turn
     * @return Game
     */
    public function setTurn($turn)
    {
        $this->turn = $turn;
        return $this;
    }

    /**
     * Get turn
     *
     * @return integer 
     */
    public function getTurn()
    {
        return $this->turn;
    }

    /**
     * Set game_field
     *
     * @param string $gameField
     * @return Game
     */
    public function setGameField($gameField)
    {
        $this->game_field = $gameField;
        return $this;
    }

    /**
     * Get game_field
     *
     * @return string 
     */
    public function getGameField()
    {
        return $this->game_field;
    }

    /**
     * Set start
     *
     * @param string $start
     * @return Game
     */
    public function setStart($start)
    {
        $this->start = $start;
        return $this;
    }

    /**
     * Get start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param string $end
     * @return Game
     */
    public function setEnd($end)
    {
        $this->end = $end;
        return $this;
    }

    /**
     * Get end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set won
     *
     * @param integer $won
     * @return Game
     */
    public function setWon($won)
    {
        $this->won = $won;
        return $this;
    }

    /**
     * Get won
     *
     * @return integer 
     */
    public function getWon()
    {
        return $this->won;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Game
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
}