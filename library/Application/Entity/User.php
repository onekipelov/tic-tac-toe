<?php

namespace Application\Entity;

use Doctrine\Common\Collections as Collections;
use Doctrine\ORM\Mapping as ORM;

/**
 * Application\Entity\User
 *
 * @Table(name="User")
 * @Entity
 */
class User
{
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @Column(name="name", type="string", length=64, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string $login
     *
     * @Column(name="login", type="string", length=16, precision=0, scale=0, nullable=false, unique=false)
     */
    private $login;

    /**
     * @var string $password
     *
     * @Column(name="password", type="string", length=48, precision=0, scale=0, nullable=false, unique=false)
     */
    private $password;

    /**
     * @var datetime $last_visit
     *
     * @Column(name="last_visit", type="string", precision=0, scale=0, nullable=false, unique=false)
     */
    private $last_visit;

    /**
     * @var integer $online_status
     *
     * @Column(name="online_status", type="integer", precision=0, scale=0, nullable=false, unique=false)
     */
    private $online_status;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set login
     *
     * @param string $login
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * Get login
     *
     * @return string 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set last_visit
     *
     * @param datetime $lastVisit
     * @return User
     */
    public function setLastVisit($lastVisit)
    {
        $this->last_visit = $lastVisit;
        return $this;
    }

    /**
     * Get last_visit
     *
     * @return datetime 
     */
    public function getLastVisit()
    {
        return $this->last_visit;
    }

    /**
     * Set online_status
     *
     * @param integer $onlineStatus
     * @return User
     */
    public function setOnlineStatus($onlineStatus)
    {
        $this->online_status = $onlineStatus;
        return $this;
    }

    /**
     * Get online_status
     *
     * @return integer 
     */
    public function getOnlineStatus()
    {
        return $this->online_status;
    }
}